"""
Settings for local development.

These settings are not fast or efficient, but allow local servers to be run
using the django-admin.py utility.

This file should be excluded from version control to keep the settings local.
"""

import os.path
import dotenv

from common import *

# load .env
dotenv.read_dotenv(os.path.join(BASE_DIR, '.env'))

# Run in debug mode.

DEBUG = True

TEMPLATE_DEBUG = DEBUG

# Local database settings. These should work well with http://postgresapp.com/.

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "HOST": "localhost",
        "NAME": "dumawerkz",
        "USER": "dumawerkz",
        "PASSWORD": "dumawerkz",
    },
}

INSTALLED_APPS += (
    'django_extensions',
    'debug_toolbar',
)

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

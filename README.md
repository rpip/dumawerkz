# DumaWerkz

It just werkz!

## How to run

```bash
$ virtualenv venv
$ source venv/bin/activate
$ pip install -r requirements.txt
$ ./manage.py syncdb
$ ./manage.py runserver # foreman start web
```

## Populating the database

You can populate the database with some fake data using the
**generate_fakedata** sub-command. The **--persist** argument ensures that the
same user is used for each job application and also when bookmarking jobs. If
absent, a new user is generated each time a job application or a bookmark is made.

```bash
$ ./manage.py generate_fakedata --users=200 --jobs=1000 --job_applications=10 bookmarks=10 --persist
```

To create 100 job applications and 10 job bookmarks using the same username
'mawuli', do the following:

```bash
$ ./manage.py generate_fakedata --job_applications=100 --bookmarks=30 --user=mawuli --persist
```

## Environment variables

Edit the .env file to set the application environment variables

These are common between environments. The `ENVIRONMENT` variable loads the
correct settings, possible values are: `DEVELOPMENT`, `STAGING`, `PRODUCTION`.

```
ENVIRONMENT='DEVELOPMENT'
DJANGO_SECRET_KEY='dont-tell-eve'
DJANGO_DEBUG='yes'
DJANGO_TEMPLATE_DEBUG='yes'
```

## Deployment

```bash
$ heroku create
$ heroku addons:add heroku-postgresql:dev
$ heroku pg:promote DATABASE_URL
$ heroku config:set ENVIRONMENT=PRODUCTION
$ heroku config:set DJANGO_SECRET_KEY=RANDOM_SECRET_KEY_HERE
```

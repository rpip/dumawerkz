from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings

from dumawerkz.apps.jobs.views import HomePageView

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', HomePageView.as_view(), name='homepage'),
    # admin
    url(r'^admin/', include(admin.site.urls)),
)

# Auto-add the applications.
for app in settings.LOCAL_APPS:
    app_name = app.split('.')[-1]
    urlpatterns += patterns('',url(r'^{0}/'.format(app_name),\
                                   include(app + '.urls', namespace=app_name, app_name=app_name)),
    )

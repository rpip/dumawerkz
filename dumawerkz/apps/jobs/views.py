from django.contrib.auth.decorators import login_required
from django.shortcuts import render, render_to_response, get_object_or_404
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.views.generic.list import ListView
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView
from django.views.generic import DetailView
from django.contrib import messages
from dumawerkz.util.tools import LoginRequiredMixin

from .models import (
    Job, JobApplication, Bookmark
)
from .forms import JobForm

class HomePageView(TemplateView):

    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super(HomePageView, self).get_context_data(**kwargs)
        context['table_title'] = 'Latest Jobs'
        context['column_title'] = 'Title'
        context['object_view_url'] = 'jobs:details'
        context['object_list'] = Job.objects.all()[:100]
        return context


class JobListView(LoginRequiredMixin, TemplateView):

    template_name = 'generic/list.html'

    def get_context_data(self, **kwargs):
        context = super(JobListView, self).get_context_data(**kwargs)
        context['table_title'] = 'Jobs Posted'
        context['column_title'] = 'Title'
        if kwargs['for_user']:
            context['objects'] = Job.objects.filter(user=self.request.user)
        else:
            context['objects'] = Job.objects.all()
        return context


class JobBookmarkView(LoginRequiredMixin, TemplateView):

    template_name = 'generic/list.html'

    def get_context_data(self, **kwargs):
        context = super(JobBookmarkView, self).get_context_data(**kwargs)
        context['table_title'] = 'Jobs Bookmarked'
        context['objects'] = Bookmark.by_user(self.request.user)
        return context



class JobAppliedListView(LoginRequiredMixin, TemplateView):

    template_name = 'generic/list.html'

    def get_context_data(self, **kwargs):
        context = super(JobAppliedListView, self).get_context_data(**kwargs)
        context['table_title'] = 'Jobs Applied'
        context['objects'] = JobApplication.by_user(self.request.user)
        return context


class JobDetailView(DetailView):

    model = Job
    template_name = 'jobs/detail.html'

    def get_context_data(self, **kwargs):
        context = super(JobDetailView, self).get_context_data(**kwargs)
        context['job_applicants'] = self.get_object().applicants
        return context



class JobCreateView(LoginRequiredMixin, CreateView):
    template_name = 'jobs/create.html'
    form_class = JobForm

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.user = self.request.user
        obj.save()
        return HttpResponseRedirect('/')


@login_required
def job_application(request, pk):
    job = get_object_or_404(Job, pk=pk)
    try:
        _job = JobApplication.objects.get(user=request.user, job=job)
        messages.info(request, "You've already applied to job: %s" % job.title)
    except JobApplication.DoesNotExist:
        job_appn = JobApplication(user=request.user, job=job)
        job_appn.save()
        messages.info(request, "successfully applied to job: %s" % job.title)
    return HttpResponseRedirect(reverse('jobs:detail', args=[str(pk)]))


@login_required
def job_bookmark(request, pk):
    job = get_object_or_404(Job, pk=pk)
    try:
        _bookmark = Bookmark.objects.get(user=request.user, job=job)
        messages.info(request, "You've already bookmarked this job: %s" % job.title)
    except Bookmark.DoesNotExist:
        bookmark = Bookmark(user=request.user, job=job)
        bookmark.save()
        messages.info(request, "Job has been bookmarked: %s" % job.title)
    return HttpResponseRedirect(reverse('jobs:detail', args=[str(pk)]))

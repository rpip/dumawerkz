"""miscellaneous functions"""
from datetime import datetime as dt, timedelta
import string
import random
from django.contrib.auth.decorators import login_required


def rand_str(size=6, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for x in range(size))

def to_mysql_timestamp(datetime_obj):
    return dt.strftime(datetime_obj, "%Y-%m-%d")

def random_mysql_timestamp():
    week = random.choice(range(1, 100))
    return to_mysql_timestamp(dt.now() + timedelta(weeks=week))


class LoginRequiredMixin(object):
    @classmethod
    def as_view(cls, **initkwargs):
        view = super(LoginRequiredMixin, cls).as_view(**initkwargs)
        return login_required(view)

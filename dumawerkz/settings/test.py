"""
Settings for local development.

These settings are not fast or efficient, but allow local servers to be run
using the django-admin.py utility.

This file should be excluded from version control to keep the settings local.
"""

import os.path
import dotenv

from common import *

# load .env
dotenv.read_dotenv(os.path.join(BASE_DIR, '.env'))

# Run in debug mode.

DEBUG = True

TEMPLATE_DEBUG = DEBUG

DATABASES = {
    'default': {
        '': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'testdb.sqlite3'),
    }
}

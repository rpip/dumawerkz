from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.db import models, utils
from django.db.models import Q
from django.db import models
import datetime

from dumawerkz.util.models import JobProfileMixin
from dumawerkz.util.tools import *



class UserProfile(JobProfileMixin):
    hash = models.CharField(max_length=8, db_index=True, editable=False)
    # areas of specialization
    bio = models.TextField(max_length=300, null=True, blank=True)
    birthday = models.DateField(null=True, blank=True)
    available_for_hire = models.BooleanField(default=True, blank=True)

    @property
    def age(self):
        if self.birthday:
            return int((datetime.date.today() - self.birthday).days / 365.25  )
        return 0

    @property
    def job_applications(self):
        return self.user.jobapplication_set.all()

    @property
    def jobs(self):
        return self.user.job_set.all()

    @classmethod
    def get_job_matches(cls, skills='', country=None,
                        salary=None, experience=None, working_hours=None):
        salary = salary if salary else 0
        skills = skills if skills else ''
        found = UserProfile.objects.filter(
            Q(country=country) | Q(salary__lte=salary) | Q(skills__contains=skills) |
            Q(experience=experience) | Q(hours=working_hours)
        )
        return found



    def __str__(self):
        return "%s" % self.user.username


def create_user_profile(sender, instance, created, **kwargs):
    if created:
        hash = rand_str(size=8)
        try:
            UserProfile.objects.create(user=instance, hash=hash)
        except utils.DatabaseError:
            pass

# signal for auto creating user profiles
post_save.connect(create_user_profile, sender=User)


# shortcut access to user profile
User.profile = property(lambda u: UserProfile.objects.get_or_create(user=u)[0])

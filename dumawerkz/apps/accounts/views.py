from django.core.urlresolvers import reverse, reverse_lazy
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.contrib import messages
from django.shortcuts import redirect
from django.views.generic.edit import CreateView, UpdateView, FormView
from django.contrib.auth.forms import PasswordChangeForm, AuthenticationForm

from dumawerkz.apps.accounts.forms import (
    CreateUserForm, LoginUserForm, ProfileForm
)
from dumawerkz.apps.accounts.models import UserProfile
from dumawerkz.util.models import DefaultsMixin, JobProfileMixin
from dumawerkz.util.tools import LoginRequiredMixin


class CreateUserView(DefaultsMixin, CreateView):
    model = User
    form_title = 'Create an Account'
    form_class = CreateUserForm
    template_name = 'generic/form.html'

    def form_valid(self, form):
        cleaned_data = form.cleaned_data
        username = email = cleaned_data.get('username')
        password = cleaned_data.get('password1')
        first_name = cleaned_data.get('first_name', '')
        redirect_to = cleaned_data.get('redirect_to', '') or reverse_lazy('accounts:profile')

        self.object = user = User.objects.create_user(username, email, password)
        user.first_name = first_name
        user.save()

        user_auth = authenticate(username=username, password=password)
        login(self.request, user_auth)

        messages.info(self.request, "Welcome to %s !" % settings.PROJECT_NAME)
        return HttpResponseRedirect(redirect_to)

    def get_context_data(self, **kwargs):
        context = super(CreateUserView, self).get_context_data(**kwargs)
        self.request.session.set_test_cookie()

        return context


class EditUserProfileView(LoginRequiredMixin, UpdateView):
    form_title = 'Update Profile'
    model = UserProfile
    form_class = ProfileForm
    template_name = 'generic/form.html'

    def get_object(self):
        return self.request.user.profile

    def form_valid(self, form):
        self.object.bio = form.cleaned_data.get('bio')
        self.object.skills = form.cleaned_data.get('skills')
        self.object.birthday = form.cleaned_data.get('birthday')
        self.object.salary = form.cleaned_data.get('salary')
        self.object.skills = form.cleaned_data.get('salary')
        self.object.experience = form.cleaned_data.get('experience')
        self.object.country = form.cleaned_data.get('country')
        self.object.available_for_hire = form.cleaned_data.get('available_for_hire')
        self.object.position = form.cleaned_data.get('position')
        self.object.save()

        messages.info(self.request, "Profile updated")
        return HttpResponseRedirect(reverse('accounts:edit_profile'))


class LoginUserView(DefaultsMixin, FormView):
    form_title = 'Login'
    form_class = LoginUserForm
    template_name = 'generic/form.html'

    def form_valid(self, form):
        cleaned_data = form.cleaned_data
        username = email = cleaned_data.get('username')
        password = cleaned_data.get('password')
        redirect_to = cleaned_data.get('redirect_to', '') or reverse_lazy('homepage')

        user_auth = authenticate(username=username, password=password)
        login(self.request, user_auth)

        messages.info(self.request, "Hello %s" % self.request.user.username)
        return HttpResponseRedirect(redirect_to)

    def get_context_data(self, **kwargs):
        context = super(LoginUserView, self).get_context_data(**kwargs)
        self.request.session.set_test_cookie()

        return context


@login_required
def delete_user(request):
    if request.user.profile.delete():
        request.user.delete()
    logout(request)
    messages.info(request, "Account deleted. Goodbye and good luck!")
    return redirect('homepage')

import random
import factory
from faker import Factory as FakerFactory
from django.contrib.auth.models import User
from dumawerkz.apps.jobs.models import Job, JobApplication, Bookmark
from dumawerkz.util.tools import random_mysql_timestamp


FAKER = FakerFactory.create()

class UserFactory(factory.django.DjangoModelFactory):
    first_name = factory.LazyAttribute(lambda x: FAKER.first_name())
    last_name = factory.LazyAttribute(lambda x: FAKER.last_name())
    username = factory.LazyAttribute(lambda x: FAKER.user_name())
    password = factory.PostGenerationMethodCall('set_password', 'secret')

    class Meta:
        model = User
        django_get_or_create = ('username',)


class JobFactory(factory.django.DjangoModelFactory):
    user = factory.LazyAttribute(lambda x: UserFactory())
    title = factory.LazyAttribute(lambda x: FAKER.job() + ': ' + \
                                  FAKER.sentence())
    status = random.choice(list(Job.STATUS.__dict__['_db_values']))
    expires = random_mysql_timestamp()

    class Meta:
        model = Job
        django_get_or_create = ('title',)


class JobApplicationFactory(factory.django.DjangoModelFactory):
    job = factory.LazyAttribute(lambda x: JobFactory())
    user = factory.LazyAttribute(lambda x: UserFactory())

    class Meta:
        model = JobApplication
        django_get_or_create = ('user','job',)


class BookmarkFactory(factory.django.DjangoModelFactory):
    job = factory.LazyAttribute(lambda x: JobFactory())
    user = factory.LazyAttribute(lambda x: UserFactory())

    class Meta:
        model = Bookmark
        django_get_or_create = ('user','job',)

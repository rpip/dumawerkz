from django.forms import ModelForm
from .models import Job, JobApplication, Bookmark


class JobForm(ModelForm):
    class Meta:
        model = Job
        exclude = ('user',)


class JobApplicationForm(ModelForm):
    class Meta:
        model = JobApplication
        fields = ('job','status',)


class Bookmark(ModelForm):
    class Meta:
        model = Bookmark
        fields = ('job',)

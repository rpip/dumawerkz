"""
WSGI config for dumawerkz project.
It exposes the WSGI callable as a module-level variable named ``application``.
For more information on this file, see
https://docs.djangoproject.com/en//howto/deployment/wsgi/
"""
import os
import dotenv


try:
    dotenv.read_dotenv(
        os.path.join(os.path.dirname(os.path.dirname(__file__)), '.env'))
except:
    pass

ENVIRONMENT = os.getenv('ENVIRONMENT')

if ENVIRONMENT == 'STAGING':
    settings = 'staging'
elif ENVIRONMENT == 'PRODUCTION':
    settings = 'production'
else:
    settings = 'development'

os.environ.setdefault("DJANGO_SETTINGS_MODULE",
                      "dumawerkz.settings.{settings}".format(settings=settings))

from django.core.wsgi import get_wsgi_application
from dj_static import Cling

if settings == 'DEVELOPMENT':
    application = get_wsgi_application()
else:
    application = Cling(get_wsgi_application())

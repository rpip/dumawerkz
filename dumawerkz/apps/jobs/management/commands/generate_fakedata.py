import os
import random
import gevent
from optparse import make_option
from faker import Factory as FakerFactory
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand, CommandError
from dumawerkz.apps.jobs.models import Job
from dumawerkz.util.tools import random_mysql_timestamp
from dumawerkz.util.factories import (
    UserFactory, JobFactory, JobApplicationFactory, BookmarkFactory
)

# gevent task timeout
TIMEOUT = 2

# fake data factory
FAKER = FakerFactory.create()


class Command(BaseCommand):
    """Generate fake data into the DB"""
    can_import_settings = True

    option_list = BaseCommand.option_list + (

        make_option('--users',
                    action='store',
                    dest='users',
                    default=0,
                    help='number of fake users to create'),

        make_option('--jobs',
                    action='store',
                    dest='jobs',
                    default=0,
                    help='number of jobs to generate'),

        make_option('--job_applications',
                    action='store',
                    dest='job_applications',
                    default=0,
                    help='number of job applicantions to create'),

        make_option('--bookmarks',
                    action='store',
                    dest='bookmarks',
                    default=0,
                    help='number of job bookmarks to create'),

        make_option('--persist',
                    action='store_true',
                    dest='persist',
                    default=True,
                    help='Use the same user for job applications'),

        make_option('--user',
                    action='store',
                    dest='user',
                    help='If persist, this set the user to use. \
                    If user is not found, then a user is created or selected \
                    at random and used for all operations'),
    )

    def handle(self, *args, **options):
        nb_users = int(options['users'])
        nb_jobs = int(options['jobs'])
        nb_job_applications = int(options['job_applications'])
        nb_bookmarks = int(options['bookmarks'])
        self.persist = options['persist']
        if self.persist:
            self.user = options['user']


        if nb_users and nb_users > 0:
            self.stdout.write("\n Creating %d users \n" % nb_users)
            self.create_users(nb_users)

        if nb_jobs and nb_jobs > 0:
            self.stdout.write("\n Creating %d jobs \n" % nb_jobs)
            self.create_jobs(nb_jobs)

        if nb_job_applications and nb_job_applications > 0:
            self.stdout.write("\n Creating %d job application \n" % \
                              nb_job_applications)
            self.create_job_applications(nb_job_applications)

        if nb_bookmarks and nb_bookmarks > 0:
            self.stdout.write("\n Creating %d job bookmarks \n" % \
                              nb_bookmarks)
            self.create_bookmarks(nb_bookmarks)


    def create_users(self, number=1):
        def _create_user(n):
            user = UserFactory()
            user.profile.bio = FAKER.job() + ': ' + FAKER.paragraph()
            user.profile.birthday = random_mysql_timestamp()
            user.profile.skills = FAKER.job() + ' ' + FAKER.job()
            user.profile.save()
            user.save()
            self.stdout.write("---> Created user %d: %s" % \
                              (n, user.username))

        users = [gevent.spawn(_create_user, n) for n in range(0, number)]
        gevent.joinall(users, timeout=TIMEOUT)

    def create_jobs(self, number=1):
        def _create_job(n):
            job = JobFactory()
            job.save()
            self.stdout.write("---> Created job %d: %s" % \
                              (n, job.title))

        jobs = [gevent.spawn(_create_job, n) for n in range(0, number)]
        gevent.joinall(jobs, timeout=TIMEOUT)

    def create_job_applications(self, number=1):
        def _create_job_application(n, user=None):
            if user:
                job_appn = JobApplicationFactory(user=user)
            else:
                job_appn = JobApplicationFactory()

            job_appn.save()
            self.stdout.write("---> Created job application %d: <User %s> and <Job %s>" % \
                              (n, job_appn.user.username, job_appn.job.title))
        # create job applications
        if self.persist:
            user = self._get_persistent_user()
            job_appns = [gevent.spawn(_create_job_application, n, user) \
                    for n in range(0, number)]
        else:
            job_appns = [gevent.spawn(_create_job_application, n) \
                         for n in range(0, number)]

        gevent.joinall(job_appns, timeout=TIMEOUT)

    def create_bookmarks(self, number=1):
        def _create_bookmark(n, user=None):
            if user:
                bookmark = BookmarkFactory(user=user)
            else:
                bookmark = BookmarkFactory()

            bookmark.save()
            self.stdout.write("---> Created bookmark %d: <User %s> and <Job %s>" % \
                              (n, bookmark.user.username, bookmark.job.title))

        # create job bookmarks
        if self.persist:
            user = self._get_persistent_user()
            bookmarks = [gevent.spawn(_create_bookmark, n, user) \
                         for n in range(0, number)]
        else:
            bookmarks = [gevent.spawn(_create_bookmark, n) \
                         for n in range(0, number)]

        gevent.joinall(bookmarks, timeout=TIMEOUT)


    def _get_persistent_user(self):
        if self.user:
            return UserFactory(username=self.user)

        return random.choice(User.objects.all())

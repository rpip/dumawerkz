"""
Django settings for bar project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

import os
import dj_database_url
from django.utils.crypto import get_random_string

from common import *

HEROKU_APP_NAME = os.getenv('HEROKU_APP_NAME', 'dumawerkz')

SITE_DOMAIN = "dumawerkz.herokuapp.com"

PREPEND_WWW = False

# Security settings.

SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")

ALLOWED_HOSTS = (
    SITE_DOMAIN,
    "{HEROKU_APP_NAME}.herokuapp.com".format(
        HEROKU_APP_NAME = HEROKU_APP_NAME,
    ),
)

# Database settings.

DATABASES = {
    "default": dj_database_url.config(default="postgresql://"),
}


# Email settings.

EMAIL_HOST = "smtp.sendgrid.net"

EMAIL_HOST_USER = os.environ.get("SENDGRID_USERNAME")

EMAIL_HOST_PASSWORD = os.environ.get("SENDGRID_PASSWORD")

EMAIL_PORT = 25

EMAIL_USE_TLS = False

SERVER_EMAIL = u"{name} <notifications@{domain}>".format(
    name = SITE_NAME,
    domain = SITE_DOMAIN,
)

DEFAULT_FROM_EMAIL = SERVER_EMAIL

EMAIL_SUBJECT_PREFIX = "[%s] " % SITE_NAME

MIDDLEWARE_CLASSES += (
    'djangosecure.middleware.SecurityMiddleware',
)

INSTALLED_APPS += (
    'djangosecure',
)

DJANGO_SESSION_COOKIE_SECURE='yes'
DJANGO_SECURE_SSL_REDIRECT='yes'
DJANGO_SECURE_HSTS_SECONDS=31536000
DJANGO_SECURE_HSTS_INCLUDE_SUBDOMAINS='yes'
DJANGO_SECURE_FRAME_DENY='yes'
DJANGO_SECURE_CONTENT_TYPE_NOSNIFF='yes'
DJANGO_SECURE_BROWSER_XSS_FILTER='yes'
DJANGO_SECURE_PROXY_SSL_HEADER='HTTP_X_FORWARDED_PROTO,https'

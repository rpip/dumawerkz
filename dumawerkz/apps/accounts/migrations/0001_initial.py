# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import geoposition.fields
import django_countries.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('country', django_countries.fields.CountryField(default=b'KE', max_length=2, null=True)),
                ('position', geoposition.fields.GeopositionField(max_length=42)),
                ('salary', models.IntegerField(null=True, verbose_name=b'Salary Expectations', blank=True)),
                ('skills', models.CharField(max_length=100, null=True, blank=True)),
                ('hours', models.IntegerField(default=0, choices=[(0, 'Not Available'), (1, 'Full Time'), (2, 'Part Time Day'), (3, 'Part Time Night'), (4, 'Weekend'), (5, 'Attachment'), (6, 'Freelance'), (7, 'Volunteer Position')])),
                ('experience', models.CharField(default=b'entry', max_length=20, choices=[(b'entry', 'Entry Level'), (b'novice', 'Novice'), (b'intermediate', 'Intermediate'), (b'expert', 'Expert')])),
                ('hash', models.CharField(max_length=8, editable=False, db_index=True)),
                ('bio', models.TextField(max_length=300, null=True, blank=True)),
                ('birthday', models.DateField(null=True, blank=True)),
                ('available_for_hire', models.BooleanField(default=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, unique=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
    ]

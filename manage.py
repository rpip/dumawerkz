#!/usr/bin/env python
import os
import sys

import dotenv

try:
    dotenv.read_dotenv(
        os.path.join(os.path.dirname(os.path.dirname(__file__)), '.env'))
except:
    pass

if __name__ == "__main__":

    ENVIRONMENT = os.getenv('ENVIRONMENT')

    if ENVIRONMENT == 'STAGING':
        settings = 'staging'
    elif ENVIRONMENT == 'PRODUCTION':
        settings = 'production'
    else:
        settings = 'development'

    os.environ.setdefault("DJANGO_SETTINGS_MODULE",
                          "dumawerkz.settings.{settings}".format(settings=settings))

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)

from django.core.validators import MaxValueValidator, MinValueValidator
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.db import models
from django_countries.fields import CountryField
from geoposition.fields import GeopositionField
from model_utils import Choices


class DefaultsMixin(object):

    def get_context_data(self, **kwargs):
        context = super(DefaultsMixin, self).get_context_data(**kwargs)

        try:
            context['form_title'] = self.form_title
        except AttributeError:
            pass

        try:
            context['form_action_url'] = self.form_action_url
        except AttributeError:
            pass

        try:
            context['form_cancel_url'] = self.form_cancel_url
        except AttributeError:
            pass

        try:
            context['table_title'] = self.table_title
        except AttributeError:
            pass

        try:
            context['column_title'] = self.column_title
        except AttributeError:
            pass

        try:
            context['object_title'] = self.object_title
        except AttributeError:
            pass

        return context


class JobProfileMixin(models.Model):
    '''
    mixin for shared model fields for JobCriteria and UserProfile models
    '''
    user = models.ForeignKey(settings.AUTH_USER_MODEL, unique=True)
    country = CountryField(default='KE', null=True)
    position = GeopositionField(null=True, blank=True)
    salary = models.IntegerField(verbose_name='Salary Expectations', null=True, blank=True)
    skills = models.CharField(max_length=100, null=True, blank=True)
    # working hour
    HOURS = Choices(
        (0, 'unavailable', _('Not Available')),
        (1, 'full_time', _('Full Time')),
        (2, 'part_time_day', _('Part Time Day')),
        (3, 'part_time_night', _('Part Time Night')),
        (4, 'weekend', _('Weekend')),
        (5, 'attachment', _('Attachment')),
        (6, 'freelance', _('Freelance')),
        (7, 'voluntary', _('Volunteer Position')),
    )
    hours = models.IntegerField(choices=HOURS, default=HOURS.unavailable)

    # level experience
    EXPERIENCE = Choices(
        ('entry', _('Entry Level')),
        ('novice', _('Novice')),
        ('intermediate', _('Intermediate')),
        ('expert', _('Expert'))
    )
    experience = models.CharField(choices=EXPERIENCE, default=EXPERIENCE.entry,
                                  max_length=20)

    class Meta:
        abstract = True

from django.conf.urls import patterns, include, url
from django.views.generic.base import TemplateView
from django.contrib.auth.decorators import login_required

from dumawerkz.apps.accounts.views import (
    CreateUserView, LoginUserView, EditUserProfileView, delete_user
)

urlpatterns = patterns('',
    url(r'login/$', LoginUserView.as_view(), {'next_page': '/'}, name='login'),
    url(r'logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}, name='logout'),
    url(r'delete/$', delete_user, name='delete'),
    url(r'signup/$', CreateUserView.as_view(), name='signup'),
    url(r'profile/$', login_required(TemplateView.as_view(template_name='accounts/profile.html')),\
                           name='profile'),
    url(r'edit/$', EditUserProfileView.as_view(), name='edit_profile'),
    url(r'profile/edit/password/$', 'django.contrib.auth.views.password_change',
        {'template_name': 'generic/form.html', 'post_change_redirect': '/accounts/profile'},
        name='password_change'),
)

from django.utils.translation import ugettext_lazy as _
from datetime import datetime as dt, timedelta
from django.core.urlresolvers import reverse
from django.db import models
from django.conf import settings
from model_utils.models import TimeStampedModel, TimeFramedModel
from model_utils import Choices

from dumawerkz.apps.accounts.models import UserProfile
from dumawerkz.util.models import JobProfileMixin
from dumawerkz.util.tools import to_mysql_timestamp


class Job(TimeStampedModel, TimeFramedModel, JobProfileMixin):
    '''Job

    Post a job and define which variables (decided by you) are mandatory
    and which are optional.
    '''
    title = models.CharField(max_length=255)
    DEFAULT_JOB_EXPIRATION = to_mysql_timestamp(dt.now() + timedelta(weeks=2))
    expires = models.DateField(null=True, blank=True, default=DEFAULT_JOB_EXPIRATION)

    # status field
    STATUS = Choices('draft','published',)
    status = models.CharField(choices=STATUS, default=STATUS.draft, max_length=20)

    def find_matches(self):
        """Simple job match algorithm.

        Use's ORM's filtering method for matching users to jobs and then sorts the results
        by number occurrences
        """
        #skills = self.skills.split(',') if self.skills else ''
        matches = UserProfile.get_job_matches(skills=self.skills,
                                              country=self.country,
                                              salary=self.salary,
                                              experience=self.experience,
                                              working_hours=self.hours
                                          )
        # order by number of appearance
        from collections import Counter
        sorted_matches = Counter(matches).most_common()
        return [k for k,v in sorted_matches]




    @property
    def applicants(self):
        return JobApplication.objects.filter(job_id=self.id)

    def get_absolute_url(self):
        return reverse('jobs:detail', args=[str(self.id)])

    def __str__(self):
        return "%s" % self.title



class JobApplication(TimeStampedModel):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    job = models.ForeignKey(Job)
    STATUS = Choices(('accepted', 'Accepted'), ('denied', 'Denied'))
    status = models.CharField(choices=STATUS, default=STATUS.denied, max_length=20)

    @classmethod
    def by_user(cls, user):
        return JobApplication.objects.filter(user=user)

    class Meta:
        unique_together = ('user', 'job',)


    def __str__(self):
        return "%s" % self.job.title


class Bookmark(TimeStampedModel):
    '''Job bookmarks

    Save the job match to be able to go back to it later
    '''
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    job = models.ForeignKey(Job)

    @classmethod
    def by_user(cls, user):
        return Bookmark.objects.filter(user=user)

    def __str__(self):
        return "%s" % self.job.title

    class Meta:
        unique_together = ('user', 'job',)

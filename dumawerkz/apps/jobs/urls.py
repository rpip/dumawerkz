from django.conf.urls import patterns, include, url
from django.contrib import admin

from .views import (
    JobListView, JobDetailView, JobCreateView,
    JobBookmarkView, JobAppliedListView, job_application, job_bookmark,
)

urlpatterns = patterns('',
    url(r'^$', JobListView.as_view(), {'for_user': False}, name='list',),
    url(r'posted/$', JobListView.as_view(), {'for_user': True}, name='posted',),
    url(r'new/$', JobCreateView.as_view(), name='new',),
    url(r'bookmarks/$', JobBookmarkView.as_view(), name='bookmarks',),
    url(r'applied/$', JobAppliedListView.as_view(), name='applied',),
    url('^(?P<pk>\d+)/$', JobDetailView.as_view(), name='detail',),
    url('^(?P<pk>\d+)/bookmark/$', job_bookmark, name='bookmark',),
    url('^(?P<pk>\d+)/apply/$', job_application, name='apply',),
)

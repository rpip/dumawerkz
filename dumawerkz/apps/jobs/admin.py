from django.contrib import admin
from .models import Job, JobApplication, Bookmark


@admin.register(Job)
class JobAdmin(admin.ModelAdmin):
    list_display = ('title', 'user', 'experience', 'salary', 'expires',)


@admin.register(JobApplication)
class JobApplicationAdmin(admin.ModelAdmin):
    list_display = ('user', 'job','status',)


@admin.register(Bookmark)
class BookmarkAdmin(admin.ModelAdmin):
    list_display = ('user', 'job',)

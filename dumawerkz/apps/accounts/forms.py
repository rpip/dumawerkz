from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.forms import AuthenticationForm
from django_countries.widgets import CountrySelectWidget

from .models import UserProfile

class LoginUserForm(AuthenticationForm):
    username = forms.CharField(label='E-mail address')
    redirect_to = forms.CharField(required=False, initial='/accounts/profile/',
                                  widget=forms.HiddenInput)

class CreateUserForm(UserCreationForm):
    username = forms.EmailField(label='E-mail address')
    redirect_to = forms.CharField(required=False, initial='/accounts/profile/',
                                  widget=forms.HiddenInput)

    class Meta(UserCreationForm.Meta):
        fields = ('first_name', 'username')


class ProfileForm(forms.ModelForm):
    email = forms.EmailField(label="Primary email",help_text='')
    first_name = forms.CharField(label="First name",help_text='')
    last_name = forms.CharField(label="Last name",help_text='')

    def __init__(self, *args, **kwargs):
        super(ProfileForm, self).__init__(*args, **kwargs)
        try:
            self.fields['email'].initial = self.instance.user.email
            self.fields['first_name'].initial = self.instance.user.first_name
            self.fields['last_name'].initial = self.instance.user.last_name
        except User.DoesNotExist:
            pass


    def save(self, *args, **kwargs):
        """
        Update the primary email address on the related User object as well.
        """
        u = self.instance.user
        u.email = self.cleaned_data['email']
        u.first_name = self.cleaned_data['first_name']
        u.last_name = self.cleaned_data['last_name']
        u.save()
        profile = super(ProfileForm, self).save(*args,**kwargs)
        return profile

    class Meta:
        model = UserProfile
        exclude = ('user',)
